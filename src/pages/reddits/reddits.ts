import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RedditService } from '../../app/services/reddit.service';
import { DetailsPage } from '../details/details';

/**
 * Generated class for the RedditsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-reddits',
  templateUrl: 'reddits.html',
})
export class RedditsPage {
  items: any;
  catagory: any;
  limit: any;
  constructor(public navCtrl: NavController, public navParams: NavParams, private redditService: RedditService) {
    this.getDefaults();
  }

  ngOnInit() {
    this.getPosts(this.catagory, this.limit);
  }

  getDefaults() {
    if(localStorage.getItem('catagory') != null) {
      this.catagory = localStorage.getItem('catagory');
    } else {
      this.catagory = 'sports';
    }

    if(localStorage.getItem('limit') != null) {
      this.limit = localStorage.getItem('limit');
    } else {
      this.limit = 10;
    }
  }

  getPosts(catagory, limit) {
    this.redditService.getPosts(catagory, limit).subscribe(response => {
      this.items = response.data.children;
    });
  }

  viewItem(item) {
    this.navCtrl.push(DetailsPage, {
      item:item
    });
  }

  changeCatagory() {
    this.getPosts(this.catagory, this.limit);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RedditsPage');
  }

}
