import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RedditsPage } from '../reddits/reddits';

/**
 * Generated class for the SettingsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  catagory: any;
  limit: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.getDefaults();
  }

  getDefaults() {
    if(localStorage.getItem('catagory') != null) {
      this.catagory = localStorage.getItem('catagory');
    } else {
      this.catagory = 'sports';
    }

    if(localStorage.getItem('limit') != null) {
      this.limit = localStorage.getItem('limit');
    } else {
      this.limit = 10;
    }
  }

  setDefaults() {
    localStorage.setItem('catagory', this.catagory);
    localStorage.setItem('limit', this.limit);
    this.navCtrl.push(RedditsPage);
  }
}
